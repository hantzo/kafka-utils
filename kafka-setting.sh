#!/bin/bash

# Cloudera Manager Credentials
USER=“admin“
PASSWORD=“admin”

# Cloudera Manager Server
CM_SERVER=“localhost:7180"

# Zookeeper Server
ZK_SERVER='localhost:2181'

# All Kafka topics
ALL_TOPICS=‘’

# Kafka Hosts. Space separated list
BROKER_HOSTS=‘host-1 host-2 host-3’

# Kafka Brokers. Space separated list
BROKER_LIST=‘host-1:9092 host-2:9092 host-3:9092’
