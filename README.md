# Kafka Utilities #

* Small utilities to help with Apache Kafka administration
* Assumes installation through Cloudera Manager using Cloudera Labs parcel.
* [George Hantzaras](https://bitbucket.org/hantzo)
* __Note:__ Tested with Cloudera Manager 5.3 and Apache Kafka 0.8.1.1

### Usage ###

* Download all files in same directory
* Add settings in kafka-setting.sh
* **Kafka-logs** gives you the message count in each topic
* **get_kafka_dirs** and **get_servers** are helper script that get the kafka data dir and brokers, respectively
* **truncate_kafka** deletes kafka topics, including cleaning up data and zookeeper directories.