#!/bin/bash

#######
. ./kafka-settings.sh
#######

if [ “${ALL_TOPICS}“ == "" ]
then
  ALL_TOPICS =`kafka-topics --list --zookeeper $ZK_SERVER`
fi

all=0
topic=""

while getopts :at:h FLAG
do
    case $FLAG in
    a) all=1;;
    t) topic=$OPTARG;;
    h) echo -e "Usage: $0 [-a all topics] [-t topic] " 1>&2; exit 1;;
    *) echo -e "$SCRIPT: error - unrecognized option $FLAG" 1>&2; exit 1;;
    \?) echo -e "Use ${BOLD}$SCRIPT -h${NORM} to see the help documentation."; exit 2;;
    esac
done

if [ $all -eq 0 ] && [ "$topic" == "" ]
then
   echo -e "Usage: $0 [-a all topics] [-t topic] " 1>&2
   exit 1
fi

if [ $all -eq 1 ]
then
   for topic in ${ALL_TOPICS}
   do
      # Delete Kafka topics
      kafka-run-class kafka.admin.DeleteTopicCommand --topic $topic --zookeeper $ZK_SERVER
     
   done
else
   kafka-run-class kafka.admin.DeleteTopicCommand --topic $topic --zookeeper $ZK_SERVER
fi

# Stop the Kafka servers
curl -u admin:admin -X POST http://${CM_SERVER}/api/v9/clusters/cluster/services/clabs_kafka/commands/stop
#curl -u admin:admin -X POST http://${CM_SERVER}/api/v9/clusters/cluster/services/clabs_kafka2/commands/stop

x=""
y=""

while [ "$x" != "STOPPED" ] || [ "$y" != "STOPPED" ]
do
   x=`curl -u admin:admin http://${CM_SERVER}/api/v9/clusters/cluster/services/ | grep "\"name\" : \"clabs_kafka\"" -A9 | grep "serviceState" | awk -F"\"" '{print $(NF-1)}'`
   #y=`curl -u admin:admin http://${CM_SERVER}/api/v9/clusters/cluster/services/ | grep "\"name\" : \"clabs_kafka\"" -A9 | grep "serviceState" | awk -F"\"" '{print $(NF-1)}'`
   sleep 20
done

echo '-------- Kafka brokers stoped --------'

#NOT REQUIRED# if [ $all -eq 1 ]
#NOT REQUIRED# then
#NOT REQUIRED#    for topic in $all_topics
#NOT REQUIRED#    do
#NOT REQUIRED#       # Cleanup zookeeper
#NOT REQUIRED#       zookeeper-client -server $ZK_SERVER rmr /broker/topics/$topic*
#NOT REQUIRED#       zookeeper-client -server $ZK_SERVER rmr /consumers/console*
#NOT REQUIRED#    done
#NOT REQUIRED# else
#NOT REQUIRED#    zookeeper-client -server $ZK_SERVER rmr /broker/topics/$topic*
#NOT REQUIRED#    zookeeper-client -server $ZK_SERVER rmr /consumers/console*
#NOT REQUIRED# fi

# Cleanup Kafka message directories
for host in ${BROKER_HOSTS}
do
   if [ $all -eq 1 ]
   then
      ssh $host rm -rf /data/kafka/{data_dup,data}/*
   else
      ssh $host rm -rf /data/kafka/{data_dup,data}/$topic-[0-9].
   fi
done

# Start the Kafka servers
curl -u admin:admin -X POST http://${CM_SERVER}/api/v9/clusters/cluster/services/clabs_kafka/commands/start
#curl -u admin:admin -X POST http://${CM_SERVER}/api/v9/clusters/cluster/services/clabs_kafka2/commands/start

k=""
m=""

while [ "$k" != "STARTED" ] || [ "$m" != "STARTED" ]
do
   k=`curl -u admin:admin http://${CM_SERVER}/api/v9/clusters/cluster/services/ | grep "\"name\" : \"clabs_kafka\"" -A9 | grep "serviceState" | awk -F"\"" '{print $(NF-1)}'`
   #m=`curl -u admin:admin http://${CM_SERVER}/api/v9/clusters/cluster/services/ | grep "\"name\" : \"clabs_kafka\"" -A9 | grep "serviceState" | awk -F"\"" '{print $(NF-1)}'`
   sleep 20
done

echo '-------- Kafka brokers started --------'
# Create topics
if [ $all -eq 0 ]
then
   kafka-topics --create --replication-factor 2 --zookeeper $ZK_SERVER --partitions 4 --topic $topic
else
   for topic in ${ALL_TOPICS}
   do
      kafka-topics --create --replication-factor 2 --zookeeper $ZK_SERVER --partitions 4 --topic $topic
      sleep 1
   done
fi


