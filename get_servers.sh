#!/bin/bash

#######
. ./kafka-settings.sh
#######

service="ZOOKEEPER"
roleName="SERVER"
defaultPort="2181"


while getopts :s:r:p:U:P:S:h FLAG
do
    case $FLAG in
    s) service=$OPTARG;;
    r) roleName=$OPTARG;;
    p) defaultPort=$OPTARG;;
    U) USER=$OPTARG;;
    P) PASSWORD=$OPTARG;;
    S) CM_SERVER=$OPTARG;;
    h) echo -e "Usage: $0 [-s service -r roleType -p defaultPort] [-U USER] [-P PASSWORD] [-S CM_SERVER_IP:PORT]" 1>&2; exit 1;;
    *) echo -e "$SCRIPT: error - unrecognized option $FLAG" 1>&2; exit 1;;
    \?) echo -e "Use ${BOLD}$SCRIPT -h${NORM} to see the help documentation."; exit 2;;
    esac
done


CM_URL="curl -s -u  ${USER}:${PASSWORD} -X GET http://${CM_SERVER}/api/v9"
clusterName=`$CM_URL/clusters | grep name |awk -F"\"" '{print $4}'`
services=`$CM_URL/clusters/cluster/services |grep "type.*${service}" -B1 |grep name |awk -F"\"" '{print $4}'`
for serviceName in $services; do
  $CM_URL/clusters/cluster/services/$serviceName/roles |grep "\"type\" : \"${roleName}\"" -B1 -A8 |grep -e name -e hostId |awk -F"\"" '{print $4}' |while read roleName && read hostId ;do
    port=`$CM_URL/clusters/$clusterName/services/$serviceName/roles/$roleName/config | grep "name\" : \"port" -A1 |grep value |awk -F"\"" '{print $4}'`
    if [ "$port" == "" ] ;then
      port=$defaultPort
    fi
    ipAddress=`$CM_URL/hosts/$hostId |grep ipAddress |awk -F"\"" '{print $4}'`
    echo "$ipAddress:$port"
  done
done
