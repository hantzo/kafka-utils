#!/bin/bash

#######
. ./kafka-settings.sh
#######
    
if [ “${ZK_SERVER}“ == "" ]
then
ZK_SERVER=`/usr/bin/icomscripts/get_servers.sh | tr "\\n" "," |sed 's/,$//g'`
fi

if [ “${BROKER_LIST}“ == "" ]
then
BROKER_LIST=`/usr/bin/icomscripts/get_servers.sh -s CLABS_KAFKA -r KAFKA_BROKER -p 9092 | tr "\\n" "," |sed 's/,$//g'`
fi

if [ “${ALL_TOPICS}“ == "" ]
then
  ALL_TOPICS=`kafka-topics --list --zookeeper $ZK_SERVER` 
fi

all=0
raw=0
topic=""
part=0

while getopts :arpt:h FLAG
do
    case $FLAG in
    a) all=1;;
    r) raw=1;;
    p) part=1;;
    t) topic=$OPTARG;;
    h) echo -e "Usage: $0 [-a all topics] [-t topic] " 1>&2; exit 1;;
    *) echo -e "$SCRIPT: error - unrecognized option $FLAG" 1>&2; exit 1;;
    \?) echo -e "Use ${BOLD}$SCRIPT -h${NORM} to see the help documentation."; exit 2;;
    esac
done

if [ $all -eq 0 ] && [ "$topic" == "" ]
then
   echo -e "Usage: $0 [-a all topics] [-t topic] " 1>&2
   exit 1
fi

header="|%-30s|%-10s|%-20s|\n"
if [ $raw -eq 1 ]
then
  columns="%s %d %d\n"
else
  columns="|%-30s|%10d|%20d|\n"
fi
if [ $raw -eq 0 ]
then
  printf '%.s=' {1..64}
  printf "\n"
  printf "$header" TOPIC_NAME PARTITION EVENTS
  printf '%.s=' {1..64}
  printf "\n"
fi
if [ $all -eq 1 ]
then
  for topic in $all_topics
  do
    if [ $part -eq 1 ]
    then
      partitions=`kafka-run-class kafka.tools.GetOffsetShell --broker-list ${BROKER_LIST} --topic $topic --time -1 `
      for line in $partitions;do
        partition=`echo $line |awk -F: '{print $2}'`
        last=`echo $line |awk -F: '{print $3}'`
        first=`kafka-run-class kafka.tools.GetOffsetShell --broker-list ${BROKER_LIST} --topic $topic --time -2 --partition $partition | awk -F: '{print $3}' `
        let diff=$last-$first
        printf "$columns" $topic $partition $diff
      done
    else
      last=`kafka-run-class kafka.tools.GetOffsetShell --broker-list ${BROKER_LIST} --topic $topic --time -1 | awk -F: '{print $3}' | awk '{ sum+=$1} END {print sum}'`
      first=`kafka-run-class kafka.tools.GetOffsetShell --broker-list ${BROKER_LIST} --topic $topic --time -2 | awk -F: '{print $3}' | awk '{ sum2+=$1} END {print sum2}'`
      let diff=$last-$first
      printf "$columns" $topic -1 $diff
    fi
  done
else
  if [ $part -eq 1 ]
  then
    partitions=`kafka-run-class kafka.tools.GetOffsetShell --broker-list ${BROKER_LIST} --topic $topic --time -1 `
    for line in $partitions;do
      partition=`echo $line |awk -F: '{print $2}'`
      last=`echo $line |awk -F: '{print $3}'`
      first=`kafka-run-class kafka.tools.GetOffsetShell --broker-list ${BROKER_LIST} --topic $topic --time -2 --partition $partition | awk -F: '{print $3}' `
      let diff=$last-$first
      printf "$columns" $topic $partition $diff
    done
  else
    last=`kafka-run-class kafka.tools.GetOffsetShell --broker-list ${BROKER_LIST} --topic $topic --time -1 | awk -F: '{print $3}' | awk '{ sum+=$1} END {print sum}'`
    first=`kafka-run-class kafka.tools.GetOffsetShell --broker-list ${BROKER_LIST} --topic $topic --time -2 | awk -F: '{print $3}' | awk '{ sum2+=$1} END {print sum2}'`
    let diff=$last-$first
    printf "$columns" $topic -1 $diff
  fi
fi


if [ $raw -eq 0 ]
then
  printf '%.s=' {1..64}
  printf "\n"
fi
